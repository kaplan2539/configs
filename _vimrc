set tabstop=4
set shiftwidth=4
set expandtab

filetype plugin on
syntax on

map m :mak <CR>

command! -nargs=1 Silent
\   execute 'silent !' . <q-args>
\ | redraw!

map R :w<CR> \|:mak <CR> \|:Silent ./restart.sh <CR>
"map ® :w<CR> \|:mak <CR> \|:! ./%:t:r <CR>
map ® :call TryToExec()<CR>

function TryToExec()
    let exec_name=expand("%:t:r")
    let exec_name=substitute(exec_name,'_',' ','g')
    echom "trying to start: ./" . exec_name
    execute "!./".exec_name
endfunction 
" Settings for `vim-plug` (https://github.com/junegunn/vim-plug)
"
" Install:
" curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
"     https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
"
" Specify a directory for plugins
" - For Neovim: ~/.local/share/nvim/plugged
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.vim/plugged')

Plug 'https://github.com/ctrlpvim/ctrlp.vim'
Plug 'https://github.com/tpope/vim-fugitive'
Plug 'https://github.com/mileszs/ack.vim'
Plug 'https://github.com/vim-airline/vim-airline'
Plug 'https://github.com/scrooloose/nerdtree'
Plug 'https://github.com/kaicataldo/material.vim'
Plug 'https://github.com/kergoth/vim-bitbake'
Plug 'https://github.com/jceb/vim-orgmode'
Plug 'https://github.com/itspriddle/vim-shellcheck'
Plug '~/.fzf'
Plug 'junegunn/fzf.vim'
Plug 'https://github.com/bazelbuild/vim-ft-bzl'
Plug 'https://github.com/christoomey/vim-tmux-navigator'
"Plug 'https://github.com/davidhalter/jedi-vim'


" Initialize plugin system
call plug#end()

" Use ag instead of ack for the ack.vim plugin
if executable('ag')
   let g:ackprg = 'ag --vimgrep'
endif


"NERDTreeSetup
map <C-n> :NERDTreeToggle<CR>
"let NERDTreeShowHidden=1
"--> use shift-i instead

set background=dark

"colorscheme material
"let g:airline_theme = 'material'
"let g:material_theme_style = 'dark'

"CtrlP setup
:let g:ctrlp_max_files=20000
:let g:ctrlp_custom_ignore='.git$|\tmp$'

autocmd FileType python map <buffer> <F9> :w<CR>:exec '!python3' shellescape(@%, 1)<CR>
autocmd FileType python imap <buffer> <F9> <esc>:w<CR>:exec '!python3' shellescape(@%, 1)<CR>

" nmap <silent> <A-Up> :wincmd k<CR>
" nmap <silent> <A-Down> :wincmd j<CR>
" nmap <silent> <A-Left> :wincmd h<CR>
" nmap <silent> <A-Right> :wincmd l<CR>
au BufNewFile,BufRead Jenkinsfile setf groovy
