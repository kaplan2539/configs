#!/bin/bash

SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

backup() {
    SRC="$1"
    TARGET_DIR="$2"
    TARGET_DIR=${TARGET_DIR:-.}

    if [[ ! -d "${TARGET_DIR}" ]]; then
        mkdir -p "${TARGET_DIR}"
    fi
    if [[ ! -d "${TARGET_DIR}" ]]; then
        echo "backup(): ERROR: cannot create ${TARGET_DIR}"
        return 1
    fi

    DST="${TARGET_DIR}/$(basename ${SRC})"

    if [[ -e "${DST}" ]] ; then
        i=0
        while [[ -e "${DST}-$i" ]] ; do
            let i++
        done
        DST="${DST}-$i"
    fi

    echo "backup(): $SRC -> $DST"
    cp -P "${SRC}" "${DST}"
}

create_link() {
    TARGET=$1
    LINK_NAME=$2

    if [[ ! -f "${TARGET}" ]];
    then
        echo "create_link(): ERROR: target ${TARGET} does not exist"
        return 1;
    fi
    
    if [[ -f "${LINK_NAME}" ]];
    then
        backup "${LINK_NAME}" "${SCRIPTDIR}/bak"
        rm ${LINK_NAME}
    fi
    
    ln -s "${TARGET}" "${LINK_NAME}"
} 

get_os() {
    UNAME=$(uname)

    case "${UNAME}" in
        "Darwin" )
            export OS=mac
            ;;
        "Linux" )
            export OS=linux
            ;;
        * )
            export OS=unknown
            ;;
    esac
}

get_os

create_link "$SCRIPTDIR/_tmux.conf" ~/.tmux.conf
create_link "$SCRIPTDIR/_vimrc" ~/.vimrc

[[ "${OS}" == "linux" ]] && [[ -z "$(which xsel)" ]] && echo "xsel not found - trying to install... " && sudo apt-get install -y xsel

echo "install tmux plugin manager..."
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
